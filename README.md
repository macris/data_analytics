__Data Management__
===================

This repository is meant for all code, scripts or useful files for [Data Managment] classes. If you don't know how to use GIT to begin to contribute please see [Contributors]. 

__Lessons__
---------------------------

* [__Lesson 1__](lessons/lesson1.md)
* [__Lesson 2__](lessons/lesson2.md)

__Learning__

* [OverTheWire](overthewire/) Learn about basic file manipulation on linux
* [VIM](http://www.openvim.com/tutorial.html) Learn the most feared command line text editor
* [Interactive Git](https://try.github.io/levels/1/challenges/1)
* [Linux Survival](http://linuxsurvival.com)


[Data Managment]:lessons
[Contributors]: contributors.md